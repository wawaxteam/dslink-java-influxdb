# dslink-java-influxdb

Java binding for the InfluxDB API.

## Running the dslink

`gradlew.bat :run -Dexec.args="-b http://localhost:8080/conn"`

## About IOT-DSA ##
[IoT-DSA](http://iot-dsa.org/)
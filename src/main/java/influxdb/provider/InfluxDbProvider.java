package influxdb.provider;

import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;

import java.util.Map;
import java.util.Map.Entry;

import org.dsa.iot.dslink.DSLink;
import org.dsa.iot.dslink.node.Node;
import org.dsa.iot.dslink.node.NodeBuilder;
import org.dsa.iot.dslink.node.NodeManager;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.node.value.ValueType;
import org.dsa.iot.dslink.util.json.JsonObject;

public class InfluxDbProvider
    extends ActionProvider
{

  /**
   * Starts building Node's tree
   *
   * @param link
   *          link
   */
  public void run(DSLink link)
  {
    NodeManager manager = link.getNodeManager();
    Node superRoot = manager.getNode("/").getNode();

    Node status = superRoot.createChild(InfluxDbConstants.STATUS).build();
    status.setValueType(ValueType.STRING);
    status.setValue(new Value(InfluxDbConstants.READY));

    NodeBuilder builder = superRoot.createChild(InfluxDbConstants.ADD_CONNECTION_ACTION);
    builder.setAction(getAddConnectionAction(manager));
    builder.build();

    configureActions(superRoot, manager);
  }

  /**
   * Initial actions assignment
   *
   * @param superRoot
   *          root
   * @param manager
   *          manager
   */
  private void configureActions(Node superRoot, NodeManager manager)
  {
    Map<String, Node> childs = superRoot.getChildren();

    for (Entry<String, Node> entry : childs.entrySet())
    {
      Node node = entry.getValue();
      if (node.getAttribute(InfluxDbConstants.ACTION) != null &&
          node.getAttribute(InfluxDbConstants.ACTION).getBool())
      {

        JsonObject object = node.getAttribute(InfluxDbConstants.CONFIGURATION).getMap();
        InfluxDbConfig config = new InfluxDbConfig();
        config.setName((String) object.get(InfluxDbConstants.NAME));
        config.setUrl((String) object.get(InfluxDbConstants.URL));
        config.setUser((String) object.get(InfluxDbConstants.USER));
        config.setPassword(node.getPassword());
        config.setDatabase((String) object.get(InfluxDbConstants.DB));
        config.setPoolable((Boolean) object.get(InfluxDbConstants.POOLABLE));
        config.setTimeout((Integer) object.get(InfluxDbConstants.DEFAULT_TIMEOUT));
        config.setNode(node);

        NodeBuilder builder = node.createChild(InfluxDbConstants.DELETE_CONNECTION);
        builder.setAction(getDeleteConnectionAction(manager));
        builder.setSerializable(false);
        builder.build();

        builder = node.createChild(InfluxDbConstants.EDIT_CONNECTION);
        builder.setAction(getEditConnectionAction(config));
        builder.setSerializable(false);
        builder.build();
        {
          builder = node.createChild(InfluxDbConstants.QUERY);
          builder.setAction(getQueryAction(config));
          builder.setSerializable(false);
          builder.build();
        }

        {
          builder = node.createChild(InfluxDbConstants.WRITE);
          builder.setAction(getWriteAction(config));
          builder.setSerializable(false);
          builder.build();
        }

        Node status = node.createChild(InfluxDbConstants.STATUS).build();
        status.setValue(new Value(InfluxDbConstants.READY));
      }
    }
  }
}

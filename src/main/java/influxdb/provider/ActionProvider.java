package influxdb.provider;

import influxdb.handlers.AddConnectionHandler;
import influxdb.handlers.DeleteConnectionHandler;
import influxdb.handlers.EditConnectionHandler;
import influxdb.handlers.QueryHandler;
import influxdb.handlers.WriteHandler;
import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;

import org.dsa.iot.dslink.node.NodeManager;
import org.dsa.iot.dslink.node.Permission;
import org.dsa.iot.dslink.node.actions.Action;
import org.dsa.iot.dslink.node.actions.EditorType;
import org.dsa.iot.dslink.node.actions.Parameter;
import org.dsa.iot.dslink.node.actions.ResultType;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.node.value.ValueType;

public class ActionProvider
{

  public Action getDeleteConnectionAction(NodeManager manager)
  {
    return new Action(Permission.READ, new DeleteConnectionHandler(manager));
  }

  public Action getEditConnectionAction(InfluxDbConfig config)
  {
    Action action = new Action(Permission.READ, new EditConnectionHandler(config));
    action.addParameter(new Parameter(InfluxDbConstants.URL, ValueType.STRING, new Value(config
        .getUrl())).setPlaceHolder("http://127.0.0.1:8086"));
    action.addParameter(new Parameter(InfluxDbConstants.USER, ValueType.STRING, new Value(config
        .getUser())));
    action.addParameter(new Parameter(InfluxDbConstants.PASSWORD, ValueType.STRING)
        .setEditorType(EditorType.PASSWORD));
    action.addParameter(new Parameter(InfluxDbConstants.DB, ValueType.STRING, new Value(config
        .getDatabase())));
    action.addParameter(new Parameter(InfluxDbConstants.DEFAULT_TIMEOUT, ValueType.NUMBER,
                                      new Value(config.getTimeout())));
    action.addParameter(new Parameter(InfluxDbConstants.POOLABLE, ValueType.BOOL, new Value(config
        .isPoolable())));
    return action;
  }

  public Action getAddConnectionAction(NodeManager manager)
  {

    Action action = new Action(Permission.READ, new AddConnectionHandler(manager));
    action.addParameter(new Parameter(InfluxDbConstants.NAME, ValueType.STRING));
    action.addParameter(new Parameter(InfluxDbConstants.URL, ValueType.STRING)
        .setPlaceHolder("http://127.0.0.1:8086"));
    action.addParameter(new Parameter(InfluxDbConstants.USER, ValueType.STRING));
    action.addParameter(new Parameter(InfluxDbConstants.PASSWORD, ValueType.STRING)
        .setEditorType(EditorType.PASSWORD));
    action.addParameter(new Parameter(InfluxDbConstants.DB, ValueType.STRING));
    action.addParameter(new Parameter(InfluxDbConstants.DEFAULT_TIMEOUT, ValueType.NUMBER));
    action.addParameter(new Parameter(InfluxDbConstants.POOLABLE, ValueType.BOOL, new Value(true)));
    return action;
  }

  public Action getQueryAction(InfluxDbConfig config)
  {
    Action action = new Action(Permission.READ, new QueryHandler(config));
    action.addParameter(new Parameter(InfluxDbConstants.SQL, ValueType.STRING));
    action.setResultType(ResultType.TABLE);
    return action;
  }

  public Action getWriteAction(InfluxDbConfig config)
  {
    Action action = new Action(Permission.WRITE, new WriteHandler(config));
    action.addParameter(new Parameter(InfluxDbConstants.SQL, ValueType.STRING));
    action.addResult(new Parameter(InfluxDbConstants.ROWS_UPDATED, ValueType.NUMBER));
    return action;
  }

}

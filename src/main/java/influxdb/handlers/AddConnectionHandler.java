package influxdb.handlers;

import org.dsa.iot.dslink.node.Node;
import org.dsa.iot.dslink.node.NodeBuilder;
import org.dsa.iot.dslink.node.NodeManager;
import org.dsa.iot.dslink.node.actions.ActionResult;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.node.value.ValueType;
import org.dsa.iot.dslink.util.handler.Handler;
import org.dsa.iot.dslink.util.json.JsonObject;

import influxdb.driver.InfluxDbConnectionHelper;
import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;
import influxdb.provider.ActionProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AddConnectionHandler
    extends ActionProvider
    implements Handler<ActionResult>
{

  private static final Logger LOG = LoggerFactory.getLogger(AddConnectionHandler.class);

  private NodeManager manager;

  public AddConnectionHandler(NodeManager manager)
  {
    this.manager = manager;
  }

  @Override
  public void handle(ActionResult event)
  {
    LOG.debug("Entering add connection handle");

    Value name = event.getParameter(InfluxDbConstants.NAME, new Value(""));
    Node child = manager.getSuperRoot().getChild(name.getString());
    Node status = manager.getSuperRoot().getChild(InfluxDbConstants.STATUS);
    if (name.getString() != null && !name.getString().isEmpty())
    {
      if (child != null)
      {
        status.setValue(new Value("connection with name " + name.getString() + " already exist"));
        return;
      }
    }
    else
    {
      status.setValue(new Value("name is empty"));
      return;
    }

    Value url = event.getParameter(InfluxDbConstants.URL, new Value(""));
    if (url.getString() == null || url.getString().isEmpty())
    {
      status.setValue(new Value("url is empty"));
      return;
    }

    Value user = event.getParameter(InfluxDbConstants.USER, new Value(""));
    Value password = event.getParameter(InfluxDbConstants.PASSWORD, new Value(""));
    
    Value database = event.getParameter(InfluxDbConstants.DB, new Value(""));
    
    Value timeout = event.getParameter(InfluxDbConstants.DEFAULT_TIMEOUT, new Value(60));
    Value poolable = event.getParameter(InfluxDbConstants.POOLABLE);

    InfluxDbConfig config = new InfluxDbConfig();
    config.setName(name.getString());
    config.setUrl(url.getString());
    config.setUser(user.getString());
    config.setPassword(password.getString().toCharArray());
    config.setDatabase(database.getString());
    config.setPoolable(poolable.getBool());
    config.setTimeout((Integer) timeout.getNumber());
    LOG.debug(config.toString());

    // create DataSource if specified
    if (poolable.getBool())
    {
      config.setDataSource(InfluxDbConnectionHelper.configureDataSource(config));
      LOG.debug(config.getDataSource().toString());
    }

    JsonObject object = new JsonObject();
    object.put(InfluxDbConstants.NAME, config.getName());
    object.put(InfluxDbConstants.URL, config.getUrl());
    object.put(InfluxDbConstants.USER, config.getUser());
    object.put(InfluxDbConstants.DB, config.getDatabase());
    object.put(InfluxDbConstants.POOLABLE, config.isPoolable());
    object.put(InfluxDbConstants.DEFAULT_TIMEOUT, config.getTimeout());

    NodeBuilder builder = manager.createRootNode(name.getString());
    builder.setAttribute(InfluxDbConstants.ACTION, new Value(true));
    builder.setAttribute(InfluxDbConstants.CONFIGURATION, new Value(object));
    builder.setPassword(password.getString().toCharArray());
    Node conn = builder.build();
    config.setNode(conn);

    Node connStatus = conn.createChild(InfluxDbConstants.STATUS).build();
    connStatus.setValueType(ValueType.STRING);
    connStatus.setValue(new Value(InfluxDbConstants.CREATED));

    builder = conn.createChild(InfluxDbConstants.DELETE_CONNECTION);
    builder.setAction(getDeleteConnectionAction(manager));
    builder.setSerializable(false);
    builder.build();

    builder = conn.createChild(InfluxDbConstants.EDIT_CONNECTION);
    builder.setAction(getEditConnectionAction(config));
    builder.setSerializable(false);
    builder.build();
    LOG.debug("Connection {} created", conn.getName());
    {
      builder = conn.createChild(InfluxDbConstants.QUERY);
      builder.setAction(getQueryAction(config));
      builder.setSerializable(false);
      builder.build();
    }

    {
      builder = conn.createChild(InfluxDbConstants.WRITE);
      builder.setAction(getWriteAction(config));
      builder.setSerializable(false);
      builder.build();
    }
    status.setValue(new Value("connection created"));
  }
}

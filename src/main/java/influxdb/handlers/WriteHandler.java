package influxdb.handlers;

import influxdb.driver.InfluxDbConnectionHelper;
import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;

import org.dsa.iot.dslink.node.actions.ActionResult;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.util.handler.Handler;
import org.influxdb.InfluxDB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antoine SAUVAGE
 */
public class WriteHandler
    implements Handler<ActionResult>
{

  private static final Logger LOG = LoggerFactory.getLogger(QueryHandler.class);

  private InfluxDbConfig config;
  
  //private NodeManager manager;

  public WriteHandler(InfluxDbConfig config)
  {
    this.config = config;
  }

  @Override
  public void handle(ActionResult event)
  {
    LOG.debug("Entering query connection handle");
    //Node status = manager.getSuperRoot().getChild(InfluxDbConstants.STATUS);

    Value value = event.getParameter(InfluxDbConstants.SQL);

    if (value != null && value.getString() != null && !value.getString().isEmpty())
    {

      String sql = value.getString();
      LOG.debug(sql);

      try
      {
        doWrite(sql, event);        
      }
      catch (Exception e)
      {
        setStatusMessage(e.getMessage());
      }
    }
    else
    {
      setStatusMessage("data is empty");
    }
  }

  private void doWrite(String query, ActionResult event) throws Exception
  {
    try
    {
      config.setDataSource(InfluxDbConnectionHelper.configureDataSource(config));
      
      //ex: query: "cpu,atag=test idle=90,usertime=9,system=1"
      config.getDataSource().write(config.getDatabase(), "default", InfluxDB.ConsistencyLevel.ONE, query);
      //status.setValue(new Value("Value(s) added"));
      //event.getParameter(InfluxDbConstants.ROWS_UPDATED, new Value(1));
    }
    catch (Exception e)
    {
      e.printStackTrace();
      setStatusMessage("error:"+ e);
    }
  }

  private void setStatusMessage(String message)
  {
    LOG.debug(message);
    config.getNode().getChild(InfluxDbConstants.STATUS).setValue(new Value(message));
  }
}

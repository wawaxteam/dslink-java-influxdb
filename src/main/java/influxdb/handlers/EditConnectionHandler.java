package influxdb.handlers;

import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;
import influxdb.provider.ActionProvider;

import org.dsa.iot.dslink.node.Node;
import org.dsa.iot.dslink.node.actions.ActionResult;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.util.handler.Handler;
import org.dsa.iot.dslink.util.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EditConnectionHandler
    extends ActionProvider
    implements Handler<ActionResult>
{

  private static final Logger LOG = LoggerFactory.getLogger(EditConnectionHandler.class);

  private InfluxDbConfig config;

  public EditConnectionHandler(InfluxDbConfig config)
  {
    this.config = config;
  }

  @Override
  public void handle(ActionResult event)
  {
    LOG.debug("Entering edit connection handle");

    Node status = config.getNode().getChild(InfluxDbConstants.STATUS);

    Value url = event.getParameter(InfluxDbConstants.URL, new Value(""));
    if (url.getString() == null || url.getString().isEmpty())
    {
      status.setValue(new Value("url is empty"));
      return;
    }

    Value user = event.getParameter(InfluxDbConstants.USER, new Value(""));
    Value password = event.getParameter(InfluxDbConstants.PASSWORD);
    Value database = event.getParameter(InfluxDbConstants.DB);
    Value timeout = event.getParameter(InfluxDbConstants.DEFAULT_TIMEOUT, new Value(60));
    Value poolable = event.getParameter(InfluxDbConstants.POOLABLE);

    LOG.debug("Old configuration is {}", config);
    config.setUrl(url.getString());
    config.setUser(user.getString());
    if (password != null)
    {
      config.setPassword(password.getString().toCharArray());
    }
    config.setPoolable(poolable.getBool());

    // create DataSource if specified
    config.setTimeout((Integer) timeout.getNumber());
    
    //setup database
    config.setDatabase(database.getString());
    
    LOG.debug("New configuration is {}", config);

    Node edit = event.getNode();
    edit.setAction(getEditConnectionAction(config));

    Node connection = config.getNode();

    JsonObject object = connection.getAttribute(InfluxDbConstants.CONFIGURATION).getMap();
    object.put(InfluxDbConstants.NAME, config.getName());
    object.put(InfluxDbConstants.URL, config.getUrl());
    object.put(InfluxDbConstants.USER, config.getUser());
    object.put(InfluxDbConstants.DB, config.getDatabase());
    object.put(InfluxDbConstants.POOLABLE, config.isPoolable());
    object.put(InfluxDbConstants.DEFAULT_TIMEOUT, config.getTimeout());
    connection.setAttribute(InfluxDbConstants.CONFIGURATION, new Value(object));
    connection.setPassword(config.getPassword());
  }
}

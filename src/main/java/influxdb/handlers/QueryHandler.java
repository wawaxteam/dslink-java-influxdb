package influxdb.handlers;

import influxdb.driver.InfluxDbConnectionHelper;
import influxdb.model.InfluxDbConfig;
import influxdb.model.InfluxDbConstants;

import java.util.List;

import org.dsa.iot.dslink.methods.StreamState;
import org.dsa.iot.dslink.node.actions.ActionResult;
import org.dsa.iot.dslink.node.actions.Parameter;
import org.dsa.iot.dslink.node.actions.table.Row;
import org.dsa.iot.dslink.node.actions.table.Table;
import org.dsa.iot.dslink.node.value.Value;
import org.dsa.iot.dslink.node.value.ValueType;
import org.dsa.iot.dslink.util.handler.Handler;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.QueryResult.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryHandler
    implements Handler<ActionResult>
{

  private static final Logger LOG = LoggerFactory.getLogger(QueryHandler.class);

  private InfluxDbConfig config;

  public QueryHandler(InfluxDbConfig config)
  {
    this.config = config;
  }

  @Override
  public void handle(ActionResult event)
  {
    LOG.debug("Entering query connection handle");

    Value value = event.getParameter(InfluxDbConstants.SQL);

    if (value != null && value.getString() != null && !value.getString().isEmpty())
    {

      String sql = value.getString();
      LOG.debug(sql);

      try
      {
        doQuery(sql, event);
      }
      catch (Exception e)
      {
        setStatusMessage(e.getMessage(), e);
      }
    }
    else
    {
      setStatusMessage("data is empty", null);
    }
  }

  private void doQuery(String query, ActionResult event)
  {
    try
    {
      config.setDataSource(InfluxDbConnectionHelper.configureDataSource(config));
      LOG.debug(config.getDataSource().toString());

      Query queryIf = new Query(query, config.getDatabase());
      QueryResult qr = config.getDataSource().query(queryIf);
      
      List<Result> res = qr.getResults();
      int columnCount = res.get(0).getSeries().get(0).getColumns().size();
      Table table = event.getTable();

      for (int i = 0; i < columnCount; i++)
      {
        ValueType type = ValueType.STRING;
        Parameter p = new Parameter(res.get(0).getSeries().get(0).getColumns().get(i), type);
        table.addColumn(p);
      }

      int size = 0;
      List<List<Object>> values = res.get(0).getSeries().get(0).getValues();
      for (int i = 0; i < values.size(); i++)
      {
        List<Object> val = values.get(i);
        Row row = new Row();
        for (int j = 0; j < val.size(); j++)
        {
          row.addValue(new Value(val.get(j).toString()));
        }
        table.addRow(row);
        size++;
      }

      String builder = "success: number of rows returned: " + size;
      setStatusMessage(builder, null);
      event.setStreamState(StreamState.CLOSED);
    }
    catch (Exception e)
    {
      setStatusMessage(e.getMessage(), e);
      e.printStackTrace();
    }
  }

  private void setStatusMessage(String message, Exception e)
  {
    if (e == null)
    {
      LOG.debug(message);
    }
    else
    {
      LOG.warn(message, e);
    }
    config.getNode().getChild(InfluxDbConstants.STATUS).setValue(new Value(message));
  }
}

package influxdb;

import influxdb.provider.InfluxDbProvider;

import org.dsa.iot.dslink.DSLink;
import org.dsa.iot.dslink.DSLinkFactory;
import org.dsa.iot.dslink.DSLinkHandler;
import org.dsa.iot.dslink.util.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Antoine SAUVAGE
 */
public class Main
    extends DSLinkHandler
{

  private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

  @Override
  public JsonObject getLinkData()
  {
    JsonObject obj = new JsonObject();
    obj.put("dslinkInfluxdb", true);
    return obj;
  }

  @Override
  public boolean isResponder()
  {
    return true;
  }

  @Override
  public void preInit()
  {
    String path = getWorkingDir().getAbsolutePath();
    LOGGER.info("Current working directory: {}", path);
  }

  @Override
  public void onResponderInitialized(DSLink link)
  {
    InfluxDbProvider provider = new InfluxDbProvider();
    provider.run(link);
    LOGGER.info("DSLink InfluxDB initialized");
  }

  @Override
  public void onResponderConnected(DSLink link)
  {
    LOGGER.info("DSLink InfluxDB connected");
  }

  @Override
  public void onResponderDisconnected(DSLink link)
  {
    LOGGER.info("Oh no! The connection to the broker is lost");
  }

  public static void main(String[] args)
  {
    //setLogLevel(LogLevel.DEBUG);
    DSLinkFactory.start(args, new Main());
  }
}

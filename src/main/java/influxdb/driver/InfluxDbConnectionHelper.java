package influxdb.driver;

import influxdb.model.InfluxDbConfig;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InfluxDbConnectionHelper
{
  private static final Logger LOG = LoggerFactory.getLogger(InfluxDbConnectionHelper.class);

  public static InfluxDB configureDataSource(InfluxDbConfig config)
  {
    InfluxDB dataSource = null;
    try
    {
      dataSource =
          InfluxDBFactory.connect(config.getUrl(), config.getUser(), new String(config.getPassword()));
      LOG.debug("Ping request: " + dataSource.ping().getResponseTime());
      return dataSource;
    }
    catch (Exception e)
    {
      e.printStackTrace();
      return null;
    }

  }
}